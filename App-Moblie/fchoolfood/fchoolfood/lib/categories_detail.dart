import 'package:fchoolfood/chat_screen.dart';
import 'package:fchoolfood/models/chat_user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/api/productapi.dart';
import 'package:fchoolfood/customNavBar.dart';
import 'package:fchoolfood/helper.dart';
import 'package:fchoolfood/item_screen.dart';
import 'package:fchoolfood/profile/profile_screen.dart';
import 'package:fchoolfood/widgets/items_widget.dart';
import 'package:provider/provider.dart';
import 'package:fchoolfood/single_item_page.dart';

import 'models/FoodProduct.dart';

class CategoriesDetail extends StatefulWidget {
  final int id;

  const CategoriesDetail({super.key, required this.id});

  @override
  State<CategoriesDetail> createState() => _CategoriesDetailState();
}

class _CategoriesDetailState extends State<CategoriesDetail> {
  String img =
      "https://firebasestorage.googleapis.com/v0/b/flutter-8c9ad.appspot.com/o/";
  String img1 = ".png?alt=media&token=";
  bool _isPressed = false;
  List foods = [
    "Hot Deals",
    "Rice",
    "Snack",
    "Cake",
    "Soda",
    "Water",
    "Other",
  ];
  List<Color> bgColors = [
    Color(0xFFFFFFFF),
    Color(0xFFFFFFFF),
    Color(0xFFFFFFFF),
    Color(0xFFFFFFFF),
    Color(0xFFFFFFFF),
    Color(0xFFFFFFFF),
    Color(0xFFFFFFFF),
  ];

  List food2 = ["Chicken Burger", "Cheese Pizza"];

  List food3 = ["Happy combos", "Breakfast combo", "Party Combos"];

  @override
  Widget build(BuildContext context) {
    List<Results> postData = [];
    final resultProvider = Provider.of<ResultProvider>(context);
    for (var item in resultProvider.results) {
      postData.add(item);
    }
    return Scaffold(
      key: UniqueKey(),
      // Ẩn màu nền của bottomNavigationBar
      body: SafeArea(
        child: Stack(
          children: [
            Material(
              color: Colors.white,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 5,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "   WELCOME TO",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black26,
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: [
                                  // Icon(
                                  //   Icons.location_on,
                                  //   color: Colors.red,
                                  // ),
                                  Text(
                                    "FFood Ordering",
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Icon(
                                    Icons.shopping_cart,
                                    color: Colors.red,
                                  ),
                                ],
                              )
                            ],
                          ),
                          Stack(
                            children: [
                              InkWell(
                                onTap: () {
                                  ChatUser a = new ChatUser(image: "image", about: "about", name: "name", createdAt: "createdAt", isOnline: true, id: "1", lastActive: "lastActive", email: "email", pushToken: "pushToken");
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      // builder: (context) => ProfileScreen()),
                                        builder: (context) => ChatScreen(user:a ,)),
                                  );
                                },
                                child: Container(
                                  height: 50,
                                  width: 50,
                                  margin: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/message.png"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 40,
                                child: Container(
                                  margin: EdgeInsets.all(5),
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.white, width: 3),
                                    color: Colors.red,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 1.4,
                          padding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                          decoration: BoxDecoration(
                            color: Colors.grey.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(
                            child: TextFormField(
                              keyboardType: TextInputType.none,
                              decoration: InputDecoration(
                                hintText: "Search your food here..",
                                border: InputBorder.none,
                                prefixIcon: Icon(Icons.search),
                              ),
                              onTap: () {
                                Future.delayed(Duration(milliseconds: 200))
                                    .then((_) {
                                  showSearch(
                                      context: context,
                                      delegate: CustomSearch());
                                });
                              },
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {

                          },
                          child: Container(
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Icon(
                              Icons.filter_list,
                              color: Colors.white,
                              size: 28,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Categories",
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          TextButton(
                              onPressed: () {},
                              child: Text(
                                "See All",
                                style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold,
                                ),
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 85,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: foods.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          CategoriesDetail(id: index)),
                                );
                              },
                              child: Container(
                                width: 80,
                                margin: EdgeInsets.only(left: 10),
                                decoration: BoxDecoration(
                                  color: Colors.grey[300],
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Image.asset(
                                      "assets/images/${foods[index]}.png",
                                      height: 50,
                                      width: 70,
                                    ),
                                    Text(
                                      foods[index],
                                      style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 15,
                                        color: Colors.black87,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          }),
                    ),
                    ItemsWidget(
                      id: widget.id,
                    ),
                    SizedBox(
                      height: 560,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
                bottom: 0,
                left: 0,
                child: CustomNavBar(
                  key: ValueKey('someValue'),
                  home: true,
                ))
          ],
        ),
      ),
    );
  }
}

class CustomSearch extends SearchDelegate {
  List<String> allData = [];

  void addData(BuildContext context) {
    final resultProvider = Provider.of<ResultProvider>(context, listen: false);
    for (var item in resultProvider.results) {
      allData.add(item.name.toString());
    }
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    allData.clear();
    addData(context);
//TODO:
    return [
      IconButton(
          onPressed: () {
            query = "";
          },
          icon: const Icon(Icons.clear))
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: const Icon(Icons.arrow_back));
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    List<String> matchQuery = [];
    for (var item in allData) {
      if (item.toLowerCase().contains(query.toLowerCase()))
        matchQuery.add(item);
    }
    return ListView.builder(
        itemCount: matchQuery.length,
        itemBuilder: (context, index) {
          var result = matchQuery[index];
          return ListTile(
            title: Text(result),
            onTap: () {
              final resultProvider =
                  Provider.of<ResultProvider>(context, listen: false);
              for (var item in resultProvider.results) {
                if (item.name == result) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SingleItemPage(
                                idItem: item,
                              )));
                }
              }
            },
          );
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    List<String> matchQuery = [];
    for (var item in allData) {
      if (item.toLowerCase().contains(query.toLowerCase()))
        matchQuery.add(item);
    }
    return ListView.builder(
        itemCount: matchQuery.length,
        itemBuilder: (context, index) {
          var result = matchQuery[index];
          return ListTile(
            title: Text(result),
            onTap: () {
              final resultProvider =
                  Provider.of<ResultProvider>(context, listen: false);
              for (var item in resultProvider.results) {
                if (item.name == result) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SingleItemPage(
                                idItem: item,
                              )));
                }
              }
            },
          );
        });
  }
}

import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'package:fchoolfood/provider/internet_provider.dart';
import 'package:fchoolfood/provider/sign_in_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:fchoolfood/screens/pratice.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/routes.dart';
import 'package:fchoolfood/screens/splash/splash_screen.dart';
import 'package:fchoolfood/theme.dart';
import 'package:fchoolfood/data/notifiers/notifiers.dart';
import 'package:fchoolfood/api/post.dart';
import 'package:fchoolfood/cart/components/check_out_card.dart';
import 'package:fchoolfood/categories_detail.dart';
import 'package:fchoolfood/history_order.dart';
import 'package:fchoolfood/home_screen.dart';
import 'package:fchoolfood/item_screen.dart';
import 'package:fchoolfood/models/Cart.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:fchoolfood/profile/profile_screen.dart';
import 'package:fchoolfood/single_item_page.dart';
import 'package:fchoolfood/widgets/notification_screen.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/api/productapi.dart';
import 'package:fchoolfood/cart/cart_screen.dart';
import 'package:fchoolfood/home_nav_bar.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

void main() async {
  // flutter run --no-sound-null-safety
  // initialize the application
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = MyHttpOverrides();
  AwesomeNotifications().initialize(
    null,
    [
      NotificationChannel(
          channelKey: "basic_channel",
          channelName: "Basic notifications",
          channelDescription: "channelDescription")
    ],
    debug: true,
  );
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late StreamSubscription<ConnectivityResult> subscription;

  @override
  void initState() {
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        isConnectedNotifier.value = false;
      } else {
        isConnectedNotifier.value = true;
      }
    });
    AwesomeNotifications().isNotificationAllowed().then((isAllowed){
      if(!isAllowed){
        AwesomeNotifications().requestPermissionToSendNotifications();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: ((context) => SignInProvider()),
        ),
        ChangeNotifierProvider(
          create: ((context) => InternetProvider()),
        ),
        ChangeNotifierProvider(create: (_) => ResultProvider()),
        ChangeNotifierProvider(create: (_) => CartProvider()),
        ChangeNotifierProvider(create: (_) => ItemProvider()),
      ],
      child: MaterialApp(

        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        // theme: theme(),
        initialRoute: SplashScreen.routeName,
        routes: routes,
      ),
    );
  }
}

class MyStrings {
  static const String errorText = "Page Not Found!";
  static const String logoText = "Logo";
  static const String error404Text = "404";
  static const String goHomeText = "Loading Page";
  static const String mainErroText =
      "We\'re sorry, the page you requested\ncould not be found. Please check \nyour internet connection and try \nreloading the app page!";
}

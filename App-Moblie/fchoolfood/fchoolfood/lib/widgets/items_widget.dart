import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/api/productapi.dart';
import 'package:fchoolfood/models/Cart.dart';
import 'package:fchoolfood/models/FoodProduct.dart';
import 'package:fchoolfood/single_item_page.dart';
import 'package:provider/provider.dart';
import 'package:fchoolfood/api/productapi.dart';
import 'package:fchoolfood/models/FoodProduct.dart';
import 'package:fluttertoast/fluttertoast.dart';
class ItemsWidget extends StatelessWidget {
  final int id;
  const ItemsWidget({super.key, required this.id});

  @override
  Widget build(BuildContext context) {

    String img = "https://firebasestorage.googleapis.com/v0/b/flutter-8c9ad.appspot.com/o/";
    String img1= ".png?alt=media&token=";
    List<int> items = [];
    List<int> itemss = [];
    int a = 0;
    int b = 1;
    List<Results> allData = [];
    final cartProvider = Provider.of<CartProvider>(context);
    final resultProvider = Provider.of<ResultProvider>(
          context);
    print(resultProvider.results.length);
      for (var item in resultProvider.results) {
        if(item.categoryId == id) {
          allData.add(item);
          items.add(a);
          itemss.add(b);
          a = a + 2;
          b = b + 2;
        }
      }
    return
      SingleChildScrollView(
          child: Container(
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: (allData.length)  % 2 == 1 ? (((allData.length) - 1) / 2 + 1).toInt() : ((allData.length) / 2).toInt(),
                  itemBuilder: (context, index){
    return
    Row(
    children: [
    SizedBox(
        width: 195,
        height: 221,
        child:
        Container(

          padding: EdgeInsets.symmetric(vertical: 5,horizontal: 10),
          margin: EdgeInsets.symmetric(vertical: 8, horizontal: 13),
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.3),
                  spreadRadius: 1,
                  blurRadius: 8,
                )
              ]
          ),
          child: Column(
            children: [
              InkWell(
              onTap: (){

                    Navigator.push(context, MaterialPageRoute(
                    builder: (context) =>
                    SingleItemPage(idItem: allData[items[index]],)));

              },
              child: Container(
              margin: EdgeInsets.all(10),
              child: Image.network(
                "${allData[items[index]].image}",
              width: 80,
              height: 80,
              fit: BoxFit.cover,
              ),
              ),
              ),
              Padding(padding: EdgeInsets.only(bottom: 8),
              child: Container(
              alignment: Alignment.center,
              child: Text(
              "${allData[items[index]].name}",
              style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.black,
              ),
              ),
              ),
              ),
              // Container(
              // alignment: Alignment.centerLeft,
              // child: Container(
              // alignment: Alignment.centerLeft,
              // child: Text(
              // "Hot Pizza",
              // style: TextStyle(
              // fontSize: 16,
              // color: Colors.black,
              // ),
              // ),
              // ),
              // ),
              Padding(padding: EdgeInsets.symmetric(vertical: 10),
              child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              Text("\$${allData[items[index]].price}",
              style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.black,
              ),
              ),
                GestureDetector(
                  onTap: () {
                    Fluttertoast.showToast(
                        msg: "This is Center Short Toast",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                    bool flag = false;
                    Cart a = new Cart(product: allData[items[index]], numOfItem: 1);
                    for(var item in cartProvider.cart){
                      if(a.results.id == item.results.id){
                        item.setQuantity();
                        flag = true;
                      }
                    }
                    if(!flag){
                      cartProvider.results(a);}
                  },
                  child: Icon(
                    CupertinoIcons.cart_badge_plus,
                    size: 27,
                    color: Colors.black,
                  ),
                )
              ],
              ),
              )
            ],
          ),
        )

    ),
      (index * 2 < (allData.length) - 2) ?
    SizedBox(
    width: 195,
        height: 221,
    child:
    Container(

    padding: EdgeInsets.symmetric(vertical: 5,horizontal: 10),
    margin: EdgeInsets.symmetric(vertical: 8, horizontal: 13),
    decoration: BoxDecoration(
    color: Colors.white,
    boxShadow: [
    BoxShadow(
    color: Colors.black.withOpacity(0.3),
    spreadRadius: 1,
    blurRadius: 8,
    )
    ]
    ),
    child: Column(
    children: [
    InkWell(
    onTap: (){
      Navigator.push(context, MaterialPageRoute(
          builder: (context) =>
              SingleItemPage(idItem: allData[itemss[index]],)));
    },
    child: Container(
    margin: EdgeInsets.all(10),
    child: Image.network(
      "${allData[itemss[index]].image}",
    width: 80,
    height: 80,
    fit: BoxFit.cover,
    ),
    ),
    ),
    Padding(padding: EdgeInsets.only(bottom: 8),
    child: Container(
      alignment: Alignment.center,
    child: Text(
      "${allData[itemss[index]].name}",
    style: TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: Colors.black,
    ),
    ),
    ),
    ),
    // Container(
    // alignment: Alignment.centerLeft,
    // child: Container(
    // alignment: Alignment.centerLeft,
    // child: Text(
    // "Hot Pizza",
    // style: TextStyle(
    // fontSize: 16,
    // color: Colors.black,
    // ),
    // ),
    // ),
    // ),
    Padding(padding: EdgeInsets.symmetric(vertical: 10),
    child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
    Text("\$${allData[itemss[index]].price}",
    style: TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: Colors.black,
    ),
    ),
      GestureDetector(
        onTap: () {
          Fluttertoast.showToast(
              msg: "This is Center Short Toast",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
          bool flag = false;
          Cart a = new Cart(product: allData[itemss[index]], numOfItem: 1);
          for(var item in cartProvider.cart){
            if(a.results.id == item.results.id){
              item.setQuantity();
              flag = true;
            }
          }
          if(!flag){
            cartProvider.results(a);}
        },
        child: Icon(
          CupertinoIcons.cart_badge_plus,
          size: 27,
          color: Colors.black,
        ),
      )
    ],
    ),
    )
    ],
    ),
    )
    )  : SizedBox.shrink()
    ]
    );
    }
  )
    )
      );
  }
}

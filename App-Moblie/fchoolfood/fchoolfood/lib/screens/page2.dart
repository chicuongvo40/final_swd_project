import 'package:flutter/material.dart';

class PageLog extends StatelessWidget {
  const PageLog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
  return MaterialApp(
  title: 'My Flutter App',
    debugShowCheckedModeBanner: false,
  theme: ThemeData(
  primarySwatch: Colors.blue,
  ),
  home: Scaffold(
      body: Container(
        height: 600,
          child :Stack(
            children: [
              Container(
                height: 500
                ,
                margin: EdgeInsets.only(left: 5, right:5,top: 25),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                            "assets/image/anime5.png"
                        )
                    )
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 130,
                  width: 100,
                  margin: EdgeInsets.only(left: 5, right:5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.white38,
                  ),
                  child: Container(
                    padding: EdgeInsets.only(top : 10, left: 15, right: 14),
                    child: Column(
                      children: [
                        Text("Item 5",
                          style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue),)

                      ],
                    ),
                  ),
                ),
              )
            ],
          )
      )
  ),
  );
  }
}

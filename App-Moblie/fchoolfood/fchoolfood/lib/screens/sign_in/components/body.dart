
import 'package:fchoolfood/home_screen.dart';
import 'package:fchoolfood/view_404/404.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/components/no_account_text.dart';
import 'package:fchoolfood/components/socal_card.dart';
import '../../../size_config.dart';
import 'sign_form.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import 'package:fchoolfood/provider/internet_provider.dart';
import 'package:fchoolfood/provider/sign_in_provider.dart';
import 'package:fchoolfood/utils_login/config.dart';
import 'package:fchoolfood/utils_login/next_screen.dart';
import 'package:fchoolfood/utils_login/snack_bar.dart';

class Body extends StatefulWidget {
  final Function() parentMethod;
  const Body({Key? key,required this.parentMethod}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

  class _BodyState extends State<Body> {


    final GlobalKey _scaffoldKey = GlobalKey<ScaffoldState>();

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: SizedBox(
            width: double.infinity,
            child: Padding(
              padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: SizeConfig.screenHeight * 0.04),
                    Text(
                      "Welcome Back",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: getProportionateScreenWidth(28),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "Sign in with your email and password  \nor continue with social media",
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: SizeConfig.screenHeight * 0.08),
                    SignForm(),
                    SizedBox(height: SizeConfig.screenHeight * 0.08),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SocalCard(
                          icon: "assets/icons/google-icon.svg",
                          press: () async {
                            bool isTrue = await  handleGoogleSignIn();

                            if (isTrue) {
                              var prefs;
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomeScreen(prefs: prefs)),
                                    (Route<dynamic> route) => false,
                              );
                              widget.parentMethod();
                            }
                          },
                        ),
                        SocalCard(
                          icon: "assets/icons/facebook-2.svg",
                          press: () {

                          },
                        ),
                        SocalCard(
                          icon: "assets/icons/twitter.svg",
                          press: () async {
                            bool isTrue = await handleTwitterAuth();

                            if (isTrue) {
                              var prefs;
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomeScreen(prefs: prefs)),
                                    (Route<dynamic> route) => false,
                              );
                              widget.parentMethod();
                            }
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    NoAccountText(),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }


// handling google sigin in
    Future<bool> handleGoogleSignIn() async {
      bool a = true;
      final sp = context.read<SignInProvider>();
      final ip = context.read<InternetProvider>();
      await ip.checkInternetConnection();

      if (ip.hasInternet == false) {
        openSnackbar(context, "Check your Internet connection", Colors.red);
      } else {
        await sp.signInWithGoogle().then((value) {
          if (sp.hasError == true) {
            a = false;
            openSnackbar(context, sp.errorCode.toString(), Colors.red);
          } else {
            // checking whether user exists or not
            sp.checkUserExists().then((value) async {
              if (value == true) {
                // user exists
                await sp.getUserDataFromFirestore(sp.uid).then((value) =>
                    sp
                        .saveDataToSharedPreferences()
                        .then((value) =>
                        sp.setSignIn().then((value) {
                          // handleAfterSignIn();
                        })));
              } else {
                // user does not exist
                sp.saveDataToFirestore().then((value) =>
                    sp
                        .saveDataToSharedPreferences()
                        .then((value) =>
                        sp.setSignIn().then((value) {
                          // handleAfterSignIn();
                        })));
              }
            });
          }
        });
      }
      return a;
    }

    Future<bool> handleTwitterAuth() async {
      bool a = true;
      final sp = context.read<SignInProvider>();
      final ip = context.read<InternetProvider>();
      await ip.checkInternetConnection();

      if (ip.hasInternet == false) {
        openSnackbar(context, "Check your Internet connection", Colors.red);
      } else {
        await sp.signInWithTwitter().then((value) {
          if (sp.hasError == true) {
            a = false;
            openSnackbar(context, sp.errorCode.toString(), Colors.red);
          } else {
            // checking whether user exists or not
            sp.checkUserExists().then((value) async {
              if (value == true) {
                // user exists
                await sp.getUserDataFromFirestore(sp.uid).then((value) =>
                    sp
                        .saveDataToSharedPreferences()
                        .then((value) =>
                        sp.setSignIn().then((value) {
                          handleAfterSignIn();
                        })));
              } else {
                // user does not exist
                sp.saveDataToFirestore().then((value) =>
                    sp
                        .saveDataToSharedPreferences()
                        .then((value) =>
                        sp.setSignIn().then((value) {
                          handleAfterSignIn();
                        })));
              }
            });
          }
        });
      }
      return a;
    }
    handleAfterSignIn() {
      // Future.delayed(const Duration(seconds: 1)).then((value) {
      //   nextScreenReplace(context, const page404());
      // });
    }
  }
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/models/Cart.dart';
import 'package:provider/provider.dart';
import 'package:fchoolfood/cart/components/check_out_card.dart';
import '../../../constants.dart';
import '../../../size_config.dart';
import 'package:flutter/material.dart';

class CartCard extends StatefulWidget {
  final Function() parentMethod;
  const CartCard({
    Key? key,
    required this.cart, required this.parentMethod
  }) : super(key: key);

  final Cart cart;
  @override
  _CartCardState createState() => _CartCardState();
}
  class _CartCardState extends State<CartCard> {


    @override
    Widget build(BuildContext context) {
      final cartProvider = Provider.of<CartProvider>(context);
      List<Cart> demoCarts = cartProvider.cart;
      void _addQuantity() {
        setState(() {
          Cart a = new Cart(product: widget.cart.results, numOfItem: 1);
          for (var item in cartProvider.cart) {
            if (a.results.id == item.results.id) {
              item.setQuantity();
              print("Da them");
            }
          }
        });
      }
      void _subQuantity() {
        setState(() {
          Cart a = new Cart(product: widget.cart.results, numOfItem: 1);
          for (var item in cartProvider.cart) {
            if (a.results.id == item.results.id) {
              item.setsubQuantity();
              print("Da tru");
            }
          }
        });
      }
      return Row(
        children: [
          SizedBox(
            width: 88,
            child: AspectRatio(
              aspectRatio: 0.88,
              child: Container(
                padding: EdgeInsets.all(getProportionateScreenWidth(10)),
                decoration: BoxDecoration(
                  color: Color(0xFFF5F6F9),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Image.network(widget.cart.results.image.toString()),
              ),
            ),
          ),
          SizedBox(width: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.cart.results.name.toString(),
                style: TextStyle(color: Colors.black, fontSize: 16),
                maxLines: 2,
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  IconButton(
                    icon: Icon(Icons.add_circle_outline_rounded),
                    onPressed: () {
                      _addQuantity();
                      widget.parentMethod();
                    },
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: " x${widget.cart.numOfItem}",
                            style: Theme
                                .of(context)
                                .textTheme
                                .bodyText1),
                      ],
                    ),
                  ),
                  widget.cart.quantity > 1 ? IconButton(
                    icon: Icon(CupertinoIcons.minus_circle),
                    onPressed: () {
                      _subQuantity();
                      widget.parentMethod();
                    },
                  ) : SizedBox.shrink(),
                  Text.rich(
                    TextSpan(
                        text: " \$${ widget.cart.product.price! *  widget.cart.quantity }",
                        style: TextStyle(
                            color: Colors.orange,
                            fontWeight: FontWeight.bold
                        )),
                  ),
                ],
              )
            ],
          )
        ],
      );
    }

  }
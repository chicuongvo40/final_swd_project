import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:fchoolfood/provider/sign_in_provider.dart';
import 'package:fchoolfood/screens/sign_in/sign_in_screen.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/api/post.dart';
import 'package:fchoolfood/api/productapi.dart';
import 'package:fchoolfood/home_screen.dart';
import 'package:fchoolfood/models/Cart.dart';
import 'package:fchoolfood/models/FoodProduct.dart';
import 'package:fchoolfood/widgets/payment.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_paypal/flutter_paypal.dart';
import 'package:awesome_notifications/awesome_notifications.dart';

class PaymentConfirm extends StatefulWidget {
  final int total;

  const PaymentConfirm({super.key, required this.total});

  @override
  State<PaymentConfirm> createState() => _PaymentConfirmState();
}

class _PaymentConfirmState extends State<PaymentConfirm> {
  int _type = 1;
  int deli = 1;


  void _handleRadio(Object? e) => setState(() {
        _type = e as int;
        deli = e as int;
      });

  @override
  void initState() {
    super.initState();
  }
  triggerNotification(){
    AwesomeNotifications().createNotification(
        content: NotificationContent(
            id: 1,
            channelKey: "basic_channel",
        title: "Payment Success",
        body: "Thanks you for your choice"
        ) );
  }
  @override
  Widget build(BuildContext context) {
    late bool _isSignedIn;
    _isSignedIn = context.read<SignInProvider>().isSignedIn;
    final cartProvider = Provider.of<CartProvider>(context);
    final resultProvider = Provider.of<ItemProvider>(context);
    List<Cart> demoCarts = [];
    List<OrderDetails> demo = [];
    for (var item in cartProvider.cart) {
      OrderDetails a = new OrderDetails(finalAmount: item.results.price,
          quantity: item.quantity, productInMenuId: item.results.id);
      demo.add(a);
      demoCarts.add(item);
    }
    Size size = MediaQuery.of(context).size;
    return
      _isSignedIn == true ?
      Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.all(30),
            child: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "PAYMENT CONFIRM",
                    textAlign: TextAlign.start,
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: "Phonenumber",
                      hintText: "Enter your phonenumber",
                      // If  you are using latest version of flutter then lable text and hint text shown like this
                      // if you r using flutter less then 1.20.* then maybe this is not working properly
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Container(child: DropDown()),
                  SizedBox(
                    height: 200,
                  ),
                  Container(
                    width: size.width,
                    height: 55,
                    decoration: BoxDecoration(
                      border: _type == 1
                          ? Border.all(width: 1, color: Colors.black)
                          : Border.all(width: 0.3, color: Colors.grey),
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.transparent,
                    ),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.only(right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Radio(
                                  value: 1,
                                  groupValue: _type,
                                  onChanged: _handleRadio,
                                  activeColor: Colors.blueGrey),
                              Text(
                                "Delivery Fee",
                                style: _type == 1
                                    ? TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black)
                                    : TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.grey),
                              )
                            ]),
                            Image.asset(
                              "assets/images/banner.jpg",
                              width: 100,
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: size.width,
                    height: 55,
                    decoration: BoxDecoration(
                      border: _type == 2
                          ? Border.all(width: 1, color: Colors.black)
                          : Border.all(width: 0.3, color: Colors.grey),
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.transparent,
                    ),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.only(right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Radio(
                                  value: 2,
                                  groupValue: _type,
                                  onChanged: _handleRadio,
                                  activeColor: Colors.blueGrey),
                              Text(
                                "Get at store",
                                style: _type == 2
                                    ? TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black)
                                    : TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.grey),
                              )
                            ]),
                            Image.asset(
                              "assets/images/banner.jpg",
                              width: 100,
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Sub-total",
                        style: TextStyle(
                            fontWeight: FontWeight.w100,
                            fontSize: 15,
                            color: Colors.grey),
                      ),
                      Text(
                        "\$${widget.total}",
                        style: TextStyle(
                            fontWeight: FontWeight.w100,
                            fontSize: 15,
                            color: Colors.red),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  deli == 1
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Shipping fee",
                              style: TextStyle(
                                  fontWeight: FontWeight.w100,
                                  fontSize: 15,
                                  color: Colors.red),
                            ),
                            Text(
                              "\$15",
                              style: TextStyle(
                                  fontWeight: FontWeight.w100,
                                  fontSize: 15,
                                  color: Colors.black),
                            ),
                          ],
                        )
                      : SizedBox.shrink(),
                  Container(
                    width: size.width,
                    height: 1,
                    color: Colors.grey,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Total Payment",
                        style: TextStyle(
                            fontWeight: FontWeight.w100,
                            fontSize: 15,
                            color: Colors.red),
                      ),
                      deli == 1
                          ? Text(
                              "\$${widget.total + 15}",
                              style: TextStyle(
                                  fontWeight: FontWeight.w100,
                                  fontSize: 15,
                                  color: Colors.black),
                            )
                          : Text(
                              "\$${widget.total}",
                              style: TextStyle(
                                  fontWeight: FontWeight.w100,
                                  fontSize: 15,
                                  color: Colors.black),
                            )
                    ],
                  ),
                  SizedBox(
                    height: 70,
                  ),
                  GestureDetector(
                    onTap: ()  {
                      if (deli == 2) {
                        triggerNotification();
                        Item a = new Item(
                            orderDetails: demo,
                            totalAmount: cartProvider.total);
                        createItem(a);

                        // var resultProvider = Provider.of<ItemProvider>(context);
                        cartProvider.clearCart();
                        cartProvider.clearItem();
                      } else {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) => UsePaypal(
                                sandboxMode: true,
                                clientId:
                                    "AXPqGKu9uXacMf3A787p0vyVk4JVtwVf29Z9b3Dc3HSW1KWRT1yDO2y4b_VqCbN-QEmPfRUvJYYQJDIA",
                                secretKey:
                                    "EMBqe-D1lIdemxXeJk-AHRIWYGTXFUxxRb6T1P42vdt6yV5BotlqycGK5VQ2qD_GSg3NwKnHWO6ZKkMj",
                                returnURL: "https://samplesite.com/return",
                                cancelURL: "https://samplesite.com/cancel",
                                transactions: const [
                                  {
                                    "amount": {
                                      "total": '10.12',
                                      "currency": "USD",
                                      "details": {
                                        "subtotal": '10.12',
                                        "shipping": '0',
                                        "shipping_discount": 0
                                      }
                                    },
                                    "description":
                                        "The payment transaction description.",
                                    // "payment_options": {
                                    //   "allowed_payment_method":
                                    //       "INSTANT_FUNDING_SOURCE"
                                    // },
                                    "item_list": {
                                      "items": [
                                        {
                                          "name": "A demo product",
                                          "quantity": 1,
                                          "price": '10.12',
                                          "currency": "USD"
                                        }
                                      ],

                                      // shipping address is not required though
                                      "shipping_address": {
                                        "recipient_name": "Jane Foster",
                                        "line1": "Travis County",
                                        "line2": "",
                                        "city": "Austin",
                                        "country_code": "US",
                                        "postal_code": "73301",
                                        "phone": "+00000000",
                                        "state": "Texas"
                                      },
                                    }
                                  }
                                ],
                                note:
                                    "Contact us for any questions on your order.",
                                onSuccess: (Map params) async {
                                  print("onSuccess: $params");
                                },
                                onError: (error) {
                                  print("onError: $error");
                                },
                                onCancel: (params) {
                                  print('cancelled: $params');
                                }),
                          ),
                        );
                        triggerNotification();
                        cartProvider.clearCart();
                        cartProvider.clearItem();

                      }
                    },
                    child: Container(
                      height: 45,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.orange,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Center(
                        child: Text(
                          "Check out",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                    child: TextButton(
                      onPressed: () {
                        var prefs ;
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomeScreen(prefs: prefs)),
                        );
                      },
                      child: Text(
                        "Continue Shopping",
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w600,
                            color: Colors.black54),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    ) : Scaffold(
        body: SignInScreen(),
      );
  }
}
